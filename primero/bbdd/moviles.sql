DROP DATABASE IF EXISTS moviles;
CREATE DATABASE IF NOT EXISTS moviles;
USE moviles;
CREATE TABLE clientes (
    dni CHAR(9),
    nombre CHAR(25),
    apellidos CHAR(50),
    telefono VARCHAR(9) DEFAULT '000000000',
    email VARCHAR(255),
    PRIMARY KEY (dni)
);
CREATE TABLE tiendas (
    nombre CHAR(35),
    provincia ENUM(
        'Alava',
        'Albacete',
        'Alicante',
        'Almeria',
        'Asturias',
        'Avila',
        'Badajoz',
        'Barcelona',
        'Burgos',
        'Caceres',
        'Cadiz',
        'Cantabria',
        'Castellon',
        'Ciudad Real',
        'Cordoba',
        'La Coruña',
        'Cuenca',
        'Gerona',
        'Granada',
        'Guadalajara',
        'Guipuzcoa',
        'Huelva',
        'Baleares',
        'Jaen',
        'Leon',
        'Lerida',
        'Lugo',
        'Madrid',
        'Malaga',
        'Murcia',
        'Navarra',
        'Orense',
        'Palencia',
        'Las Palmas',
        'Pontevedra',
        'La Rioja',
        'Salamanca',
        'Segovia',
        'Sevilla',
        'Soria',
        'Tarragona',
        'Santa Cruz de Tenerife',
        'Teruel',
        'Toledo',
        'Valencia',
        'Valladolid',
        'Vizcaya',
        'Zamora',
        'Zaragoza',
        'Ceuta',
        'Melilla'
     ),
    localidad VARCHAR(50),
    direccion VARCHAR(50),
    telefono CHAR(9),
    dia_apertura ENUM(
        'Lunes',
        'Martes',
        'Miercoles',
        'Jueves',
        'Viernes',
        'Sabado',
        'Domingo'

         ),
    dia_cierre ENUM(
        'Lunes',
        'Martes',
        'Miercoles',
        'Jueves',
        'Viernes',
        'Sabado',
        'Domingo'

    ),
    hora_apertura TIME,
    hora_cierre TIME,
    PRIMARY KEY (nombre)

);
CREATE TABLE operadoras (
    nombre CHAR(55),
    color_logo VARCHAR(6),
    porcentaje_cobertura VARCHAR(3),
    frecuencia_gsm ENUM(
        '800',
        '900',
        '1500',
        '1800',
        '2100',
        '2600',
        '3500'
    ),
    pagina_web VARCHAR(55),
    PRIMARY KEY (nombre)
);
CREATE TABLE tarifas (
    nombre VARCHAR(35),
    nombreOPERADORAS CHAR(55),
    tamaño_datos TINYINT UNSIGNED,
    tipo_datos CHAR(6),
    minutos_gratis SMALLINT UNSIGNED,
    sms_gratis TINYINT UNSIGNED,
    PRIMARY KEY (nombre, nombreOPERADORAS),
    FOREIGN KEY (nombreOPERADORAS) REFERENCES operadoras(nombre) ON DELETE NO ACTION ON UPDATE CASCADE
);
CREATE TABLE moviles (
    marca VARCHAR(25),
    modelo VARCHAR(30),
    descripcion TEXT,
    so ENUM(
        'Android',
        'IOS',
        'Windows Phone',
        'Symbian'
    ),
    ram CHAR(1),
    pulgadas_pantalla DECIMAL(2,1), ##DECIMAL(m,d) m= tamaño total, d= coma contando desde la izquierda
    camara_mpx TINYINT UNSIGNED,
    PRIMARY KEY (marca,modelo)
);
CREATE TABLE movil_libre (
    marca_MOVILES VARCHAR(25),
    modelo_MOVILES VARCHAR(30),
    precio DECIMAL(6,2),
    PRIMARY KEY (modelo_MOVILES),
    FOREIGN KEY (marca_MOVILES,modelo_MOVILES) REFERENCES moviles(marca,modelo)
);
CREATE TABLE movil_contrato (
    marca_MOVILES VARCHAR(25),
    modelo_MOVILES VARCHAR(30),
    nombre_OPERADORAS CHAR(55),
    precio DECIMAL(6,2),
    PRIMARY KEY (marca_MOVILES,modelo_MOVILES,nombre_OPERADORAS),
    FOREIGN KEY (marca_MOVILES, modelo_MOVILES) REFERENCES moviles (marca, modelo),
    FOREIGN KEY (nombre_OPERADORAS) REFERENCES operadoras (nombre)
);
CREATE TABLE ofertas (
    nombre_OPERADORAS_TARIFAS CHAR(55),
    nombre_TARIFAS VARCHAR(35),
    marca_MOVIL_CONTRATO VARCHAR(25),
    modelo_MOVIL_CONTRATO VARCHAR(30),
    PRIMARY KEY (nombre_TARIFAS, marca_MOVIL_CONTRATO, modelo_MOVIL_CONTRATO),
    FOREIGN KEY (nombre_OPERADORAS_TARIFAS, nombre_TARIFAS) REFERENCES tarifas(nombreOPERADORAS, nombre) ON DELETE NO ACTION ON UPDATE CASCADE,
    FOREIGN KEY (marca_MOVIL_CONTRATO, modelo_MOVIL_CONTRATO) REFERENCES movil_contrato(marca_MOVILES, modelo_MOVILES) ON DELETE NO ACTION ON UPDATE CASCADE
);
CREATE TABLE compras (
    dni_CLIENTE CHAR(9),
    nombre_TIENDA CHAR(35),
    marca_MOVILES_LIBRE VARCHAR(25),
    modelo_MOVILES_LIBRE VARCHAR(30),
    dia DATE,
    PRIMARY KEY (dni_CLIENTE, nombre_TIENDA, marca_MOVILES_LIBRE, modelo_MOVILES_LIBRE),
    FOREIGN KEY (dni_CLIENTE) REFERENCES clientes(dni),
    FOREIGN KEY (nombre_TIENDA) REFERENCES tiendas(nombre),
    FOREIGN KEY (marca_MOVILES_LIBRE, modelo_MOVILES_LIBRE) REFERENCES movil_libre(marca_MOVILES, modelo_MOVILES)
);
CREATE TABLE contratos(
    dni_CLIENTE CHAR(9),
    nombre_TIENDA CHAR(35),
    nombre_OPERADORAS_TARIFAS_OFERTAS CHAR(55),
    nombre_TARIFAS_OFERTAS VARCHAR(35),
    marca_MOVILES_OFERTAS VARCHAR(25),
    modelo_MOVILES_OFERTAS VARCHAR(30),
    dia DATE,
    PRIMARY KEY (dni_CLIENTE, nombre_TIENDA, nombre_OPERADORAS_TARIFAS_OFERTAS, nombre_TARIFAS_OFERTAS, marca_MOVILES_OFERTAS, modelo_MOVILES_OFERTAS),
    FOREIGN KEY (dni_CLIENTE) REFERENCES clientes(dni),
    FOREIGN KEY (nombre_TIENDA) REFERENCES tiendas(nombre),
    FOREIGN KEY (nombre_OPERADORAS_TARIFAS_OFERTAS, nombre_TARIFAS_OFERTAS, marca_MOVILES_OFERTAS, modelo_MOVILES_OFERTAS) REFERENCES ofertas(nombre_OPERADORAS_TARIFAS, nombre_TARIFAS, marca_MOVIL_CONTRATO, modelo_MOVIL_CONTRATO)
);


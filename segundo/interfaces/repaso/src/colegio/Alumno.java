package colegio;

public class Alumno {

	// Attributes
	private String dni;
	private String nombre;
	private String apellidos;
	private String nacimiento;
	private String sexo;
	private boolean repetidor;
	private Modulo[] modulos;

	// Constructor

	public Alumno(String dni, String nombre, String apellidos, String nacimiento, String sexo, boolean repetidor,
			Modulo[] modulos) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.nacimiento = nacimiento;
		this.sexo = sexo;
		this.repetidor = repetidor;
		this.modulos = modulos;
	}

	// Getters & Setters
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(String nacimiento) {
		this.nacimiento = nacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public boolean isRepetidor() {
		return repetidor;
	}

	public void setRepetidor(boolean repetidor) {
		this.repetidor = repetidor;
	}

	public Modulo[] getModulos() {
		return modulos;
	}

	public void setModulos(Modulo[] modulos) {
		this.modulos = modulos;
	}

	@Override
	public String toString() {
		System.out.print("\nInformaci�n de alumno/a " + nombre + " " + apellidos + ":\n\tModulos:");

		for (int i = 0; i < modulos.length; i++)
			System.out.println(modulos[i]);

		return "\n\tInformaci�n Personal:\n\t\tNombre Completo: " + nombre + " " + apellidos + "\n\t\tDNI: " + dni
				+ "\n\t\tFecha Nacimiento: " + nacimiento + "\n\t\tSexo: " + sexo + "\n\t\tRepite:  "
				+ Persona.conversor(repetidor);
	}

}

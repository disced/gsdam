package colegio;

public class Administracion extends Persona {

	// Attributes
	private String estudios;
	private double antiguedad;

	// Constructor
	public Administracion(String dNI, String nombre, String apellidos, double salario, String estudios,
			double antiguedad) {

		super(dNI, nombre, apellidos, salario);

		this.estudios = estudios;
		this.antiguedad = antiguedad;
	}

	// Getters & Setters
	public String getEstudios() {
		return estudios;
	}

	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}

	public double getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(double antiguedad) {
		this.antiguedad = antiguedad;
	}

	@Override
	public String toString() {
		return "\nInformaci�n de Administrativo/a " + super.getNombre() + " " + super.getApellidos() + "\n\tEstudios: "
				+ estudios + "\n\tAntiguedad: " + antiguedad + " a�os" + super.toString();
	}

}

package login;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.SystemColor;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JPasswordField;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;



public class VentanaLogin extends JFrame
{
    private JTextField     textField;
    private JPasswordField passwordField;

    public VentanaLogin ()
    {
        super( "Login" );
        inicializa();
    }

    private void inicializa ()
    {
        // Frame
        setBounds( 0, 0, 354, 269 );
        setLocationRelativeTo( null );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        setVisible( true );
        setResizable( false );
        getContentPane().setLayout( new GridLayout( 2, 0 ) );

        // Paneles
        JPanel panelDatos   = new JPanel();
        JPanel panelBotones = new JPanel();

        panelDatos.setBackground( SystemColor.inactiveCaption );

        panelBotones.setBackground( SystemColor.inactiveCaption );
        panelBotones.setLayout( new GridLayout( 1, 0 ) );

        getContentPane().add( panelDatos );
        panelDatos.setLayout( null );

        JLabel lblNewLabel_1 = new JLabel( "Usuario" );
        lblNewLabel_1.setHorizontalAlignment( SwingConstants.RIGHT );
        lblNewLabel_1.setFont( new Font( "Consolas", Font.BOLD, 19 ) );
        lblNewLabel_1.setBounds( 10, 25, 114, 35 );

        textField = new JTextField();
        textField.setBounds( 176, 24, 145, 35 );
        panelDatos.add( textField );
        textField.setColumns( 10 );

        passwordField = new JPasswordField();
        passwordField.setEchoChar( '*' );
        passwordField.setBounds( 176, 80, 145, 35 );

        JLabel lblNewLabel_1_1 = new JLabel( "Contrase\u00F1a" );
        lblNewLabel_1_1.setHorizontalAlignment( SwingConstants.RIGHT );
        lblNewLabel_1_1.setFont( new Font( "Consolas", Font.BOLD, 19 ) );
        lblNewLabel_1_1.setBounds( 20, 81, 114, 35 );
        panelDatos.add( lblNewLabel_1_1 );

        // Botones del panelBotones
        JButton btnCancelar = new JButton( "CANCELAR" );

        btnCancelar.setBackground( new Color( 255, 102, 102 ) );
        btnCancelar.setFont( new Font( "Consolas", Font.BOLD, 23 ) );
        JButton btnLogin = new JButton( "LOGIN" );

        btnLogin.setBackground( new Color( 51, 153, 255 ) );
        btnLogin.setFont( new Font( "Consolas", Font.BOLD, 23 ) );

        getContentPane().add( panelBotones );
        panelDatos.add( passwordField );
        panelDatos.add( lblNewLabel_1 );
        panelBotones.add( btnCancelar );
        panelBotones.add( btnLogin );

        dispose();
        setUndecorated( true );
        setVisible( true );

        btnCancelar.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                dispose();
            }
        } );

        btnLogin.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                guardaDatos( textField, passwordField );
            }
        } );
    }

    private void guardaDatos ( JTextField campoUsuario, JPasswordField campoContra )
    {

        String usuario;
        char[] contra;

        usuario = campoUsuario.getText();
        contra  = campoContra.getPassword();
        try
        {
            if( compruebaDatos( "usuarios.txt", usuario, contra ) )
                System.out.println( "Datos Correctos" );
            else
                System.out.println( "Datos Incorrectos" );
        }catch( IOException e )
        {
            System.out.println( "Error al comprobar el fichero" );
        }

    }

    private boolean compruebaDatos ( String nombreFichero, String usuario, char[] contra ) throws IOException
    {
        File           ficheroDatos      = new File( nombreFichero );
        String         contraIntroducida = new String( contra );

        FileReader     ficheroDatosFR    = new FileReader( ficheroDatos );
        BufferedReader ficheroDatosBR    = new BufferedReader( ficheroDatosFR );

        String         lineaLeida;
        boolean        sigue             = ficheroDatosBR.ready();
        boolean        rv                = false;
        while( sigue )
        {
            lineaLeida = ficheroDatosBR.readLine(); // Le la primera linea. (usuario)
            if( lineaLeida.equals( usuario ) ) // Si la primera linea es el usuario comprueba la contrase�a.
            {
                if( comparaDatos( usuario, contraIntroducida, lineaLeida, ficheroDatosBR.readLine() ) ) // Si es su contrase�a para el bucle y devuelve true.
                {
                    sigue = false;
                    rv    = true;
                }else // Si no es su contrase�a para el bucle y devuelve false, ya que no puede haber nombres de usuarios repetidos.
                {
                    sigue = false;
                    rv    = false;
                }
            }else
                ficheroDatosBR.readLine(); // Si la lineaLeida no es el usuario, salta la siguiente, que ser�a la contrase�a.
            sigue = ficheroDatosBR.ready(); // Comprueba que haya mas lineas para seguir leyendo.
        }

        ficheroDatosBR.close();
        ficheroDatosFR.close();
        return rv;
    }

    private boolean comparaDatos ( String usuarioIntroducido, String contraConvertida, String usuarioFichero,
            String contraFichero )
    {
        boolean rv;
        if( usuarioIntroducido.equals( usuarioFichero ) && contraConvertida.equals( contraFichero ) )
            rv = true;
        else
            rv = false;
        return rv;
    }
}

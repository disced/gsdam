package ejercicios;

public class E2 {

	public static void main(String[] args) {
		String name = "Dragos Butnariu";
		int age = 23;
		char character = ')';
		double week = 1200.11;
		boolean adult = true;

		System.out.println("My name is " + name + " I'm " + age + " years old ");
		System.out.println("Adult?: " + adult);
		System.out.println("I need a " + character);
		System.out.println("My age in weeks is: " + week);
	}

}

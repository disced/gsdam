package ejercicios;

import java.util.Random;
import java.util.Scanner;

public class N4 {

	public static void main(String[] args) {
		int size, total = 0;
		int[][] matrix;
		Scanner sc = new Scanner(System.in);
		Random rand = new Random();

		// User input
		System.out.println("Enter the size of the square matrix: ");
		size = sc.nextInt();

		matrix = new int[size][size];

		// Algorithm
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				matrix[i][j] = rand.nextInt(10);

				total += matrix[i][j];
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}

		// Output
		System.out.println("\nThe sum of matrix is = " + total);
		sc.close();
	}

}

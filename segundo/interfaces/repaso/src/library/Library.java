package library;

public class Library
{
    protected String code;
    protected String title;
    protected int    year;

    // Constructor
    public Library ( String code, String title, int year )
    {
        this.code  = code;
        this.title = title;
        this.year  = year;
    }

    // Getters
    public int getYear ()
    {
        return this.year;
    }

    public String getCode ()
    {
        return this.code;
    }

    // Metodo toString
    @Override
    public String toString ()
    {
        String str = "\tTitulo: " + title + "\n\tA�o Publicaci�n: " + year + "\n\tCodigo: " + code + "\n";
        return str;
    }

}

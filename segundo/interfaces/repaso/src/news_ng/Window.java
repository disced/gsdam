package news_ng;

import javax.swing.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;



public class Window
{
    private JFrame    frame;
    private JPanel    panel;
    private JMenu     menuMagazine;
    private JMenuItem itemElPais;
    private JMenuItem itemAbc;
    private JLabel    labelMagazine;
    private JButton   btnUpdate;
    private JLabel    labelHeadline;

    private enum MAGAZINE
    {
        ELPAIS, ABC, ANY
    };

    private MAGAZINE magazine = MAGAZINE.ANY;

    public Window ()
    {
        frame         = new JFrame( "Noticias" );
        panel         = new JPanel();
        btnUpdate     = new JButton( "Ver Titular" );

        labelMagazine = new JLabel( "Periodico Elegido" );
        labelHeadline = new JLabel( "Actualiza para ver el titular" );

        JMenuBar menuBar = new JMenuBar();
        menuMagazine = new JMenu( "Lista de Periodicos" );
        itemElPais   = new JMenuItem( "El Pais" );
        itemAbc      = new JMenuItem( "ABC" );

        // Properties
        frame.setBounds( 0, 0, 400, 250 );
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.setLocationRelativeTo( null );
        frame.setResizable( false );
        frame.setJMenuBar( menuBar );

        panel.setLayout( new BorderLayout( 0, 0 ) );

        btnUpdate.setFont( new Font( "Tahoma", Font.BOLD, 16 ) );
        btnUpdate.setForeground( SystemColor.inactiveCaptionBorder );
        btnUpdate.setBackground( SystemColor.textHighlight );

        labelMagazine.setFont( new Font( "Tahoma", Font.BOLD, 18 ) );
        labelMagazine.setHorizontalAlignment( SwingConstants.CENTER );

        labelHeadline.setFont( new Font( "Tahoma", Font.BOLD, 14 ) );
        labelHeadline.setHorizontalAlignment( SwingConstants.CENTER );

        // Adds
        frame.getContentPane().add( panel );

        panel.add( labelMagazine, BorderLayout.NORTH );
        panel.add( btnUpdate, BorderLayout.SOUTH );
        panel.add( labelHeadline, BorderLayout.CENTER );
        menuBar.add( menuMagazine );

        menuMagazine.add( itemElPais );
        menuMagazine.add( itemAbc );

        btnUpdate.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                switch (magazine)
                {
                    case ELPAIS:
                        labelHeadline.setText( getHeadline( "https://www.elpais.com", "h2.c_h, h2.headline > a" ) );
                    break;

                    case ABC:
                        labelHeadline.setText( getHeadline( "https://www.abc.es", "h3.titular, h3.xxxxl > a" ) );
                    break;
                    default:
                        labelHeadline.setText( "Selecciona un Periodico de la Lista" );
                    break;
                }
            }
        } );

        itemElPais.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                // Cambia el texto del label para saber que periodico se ha elegido
                // y guarda la constante del periodico elegido en 'magazine' para
                // su posterior uso en el switch
                labelMagazine.setText( magazineSelected( itemElPais ) );
                magazine = MAGAZINE.ELPAIS;
            }
        } );

        itemAbc.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                labelMagazine.setText( magazineSelected( itemAbc ) );
                magazine = MAGAZINE.ABC;
            }
        } );

    }

    public void setVisible ( boolean opt )
    {
        frame.setVisible( opt );
    }

    private String magazineSelected ( JMenuItem item )
    {
        // Devuelve el String del item seleccionado en el menu
        return item.getText();
    }

    private Document getWeb ( String web ) throws IOException
    {
        // Se conecta a una web y devuelve el html en
        // un objeto de tipo Document.
        Document doc;
        doc = Jsoup.connect( web ).get();
        return doc;
    }

    private String getHeadline ( String web, String elements )
    {
        // Retorna el primer texto de una web que pasamos por argumento
        // con los elementos html (los elementos con formato JSOUP)
        // Ej: "h1#prinicpal > a#titulo, a.titulo"
        // Elemento el texto de un elemento 'a' con su clase titulo y con id titulo
        // que est� dentro de un elemento h1 con clase principal
        Document webDoc;
        String   headline = "Sin Titular";
        try
        {
            webDoc   = getWeb( web );
            headline = webDoc.select( elements ).first().text();
        }catch( IOException e )
        {
            System.out.println( e.getMessage() );
        }
        return formatString( headline );
    }

    private static String formatString ( String text )
    {
        // Formatea la cadena de texto con saltos de linea
        // con formato html (para utilizar en los Label)
        int    size   = text.length();
        String textRv = "<html> ";
        for ( int i = 0; i < size; i++ )
        {
            char car = text.charAt( i );
            textRv += car;

            if( ( i == 50 || i == 51 ) && car == ' ' )
                textRv += "<br/>";
        }
        textRv += "<html/>";
        return textRv;
    }
}
package swing;

import java.awt.*;
import javax.swing.*;

public class MainFrame extends JFrame {
  private String tit = "Sin Titulo";

  public MainFrame() {
    // Un Frame tiene parecido con una mesa de un dibujante (el no dibuja en la misma mesa, usa
    // laminas o paneles en nuestro caso)

    // Pantalla y Tama�os
    setTitle(tit);
    int height, width;
    Toolkit screen = Toolkit.getDefaultToolkit(); // Devuelve que tipo de pantalla se usa
    Dimension size = screen.getScreenSize();      // Devuelve el tama�o de la pantalla. Dimensi�n tiene 2
                                                  // attr. (altura, anchura)

    height = size.height;
    width = size.width;

    setSize(width / 3, height / 3);         // El tama�o del Frame ser� 3 veces menos que la altura y
                                            // anchura de la pantalla
    setLocation(width / 3, height / 3);
    setResizable(false);


    // Hace visible JFrame
    setVisible(true);
  }

  public void setTit(String tit) {
    this.tit = tit;
  }

  public String getTit() {
    return this.tit;
  }
}

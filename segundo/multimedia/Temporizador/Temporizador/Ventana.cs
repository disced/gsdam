﻿using System;
using System.Windows.Forms;

namespace Temporizador
{
    public partial class Ventana : Form
    {
        private Reloj temp = new Reloj(1, 30);
        public Ventana()
        {
            InitializeComponent();
        }

        private void botonProgramar_Click(object sender, EventArgs e)
        {
            byte hora, minuto, segundo;
            
            hora    = byte.Parse ( textBoxHoras.Text    );
            minuto  = byte.Parse ( textBoxMinutos.Text  );
            segundo = byte.Parse ( textBoxSegundos.Text );
            int hF, mF;

            if (hora > 0 || minuto > 0 || segundo > 0)
            {
                temp.configuraTiempo(hora, minuto, segundo);
            }
            temp.pasaSegundos();
            temp.configuraApagado();
            temp.horaFutura(out  hF, out  mF);
            labelTitulo.Text      = "SE APAGA A LAS";
            labelHoraApagado.Text = hF + ":" + formatMinutes(mF);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            temp.cancelaApagado();
            labelTitulo.Text      = "CANCELADO...";
            labelHoraApagado.Text = "hh-mm";
            textBoxHoras.Text     = "0";
            textBoxMinutos.Text   = "0";
            textBoxSegundos.Text  = "0";
        }

        private string formatMinutes (int min)
        {
            string retStr = "" + min;
            if (min < 10)
                retStr = "0" + min;
            return retStr;
        }
    }
}

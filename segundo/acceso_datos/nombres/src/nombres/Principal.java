package nombres;

import java.util.Scanner;

public class Principal
{

    public static void main ( String[] args )
    {
        tablaNombres nombres = new tablaNombres( );
      
        
        int          opc;
        boolean seguir = true;
        String     menu = "\t1.- Introducir Nombres\n\t2.- Crear Fichero\n\t3.- Escribir en el Fichero\n\t4.- Leer el Fichero\n\t5.- Eliminar Nombres\n\t6.- Imprimir Nombres\n\t7.- Salir\n\n";
        Scanner  sc      = new Scanner( System.in );
        
        System.out.print( "\n\n\tNombres (Escritura, Lectura fichero)\n\n" );
        while( seguir )
        {
            System.out.println( menu +  "Opci�n" );
            opc = sc.nextInt();
            switch (opc)
            {
                case 1:
                    nombres.insertaNombre();
                break;
                case 2:
                    nombres.crearFichero();
                break;
                case 3:
                    nombres.escribeFichero();
                break;
                case 4:
                    nombres.leerFichero();
                break;
                case 5:
                    nombres.vaciarNombres();
                break;
                case 6:
                    System.out.println( nombres.imprimeNombres() );
                    break;
                case 7:
                    sc.close();
                    seguir = false;
                break;
                default:
                    System.out.println( "Opci�n No Valida" );
                break;
            }
            
        }
    
    }

}

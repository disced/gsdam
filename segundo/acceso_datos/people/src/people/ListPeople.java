package people;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ListPeople
{
    private String fileName;
    private ArrayList<Person> people; // ArrayList que guarda objetos 'Person'

    // Constructor, que reserva memoria para un ArraList<Person>
    public ListPeople ()
    {
        people = new ArrayList<>();
    }


    // Metodo interno de la clase, lo uso a la hora
    // de escribir en el fichero.
    private String printPeople ()
    {
        StringBuilder str = new StringBuilder();
        // Uso un StringBuilder para utilziar su metodo append
        // y evitar el uso de +=

        for ( Person person : people )
            str.append( person.toString() );

        return str.toString();
    }

    // Instancio nuevas personas dentro del ArrayList
    public void createPerson ( String name, String lastName, int age, String country )
    {
        people.add( new Person( name, lastName, age, country ) );
    }

    public void createFile ( String fileName )
    {
        File    file = new File( this.fileName = fileName );
        if( !file.exists() )
        {
            try
            {
                file.createNewFile();
            }catch( IOException e )
            {
                System.out.println( e.getMessage() );
            }
        }
    }

    public void writeFile ()
    {
        File file = new File( this.fileName );
        try
        {
            FileWriter     fileW  = new FileWriter     ( file );
            BufferedWriter fileBW = new BufferedWriter ( fileW );
            fileBW.write( printPeople() );
            fileBW.close();
        }catch( IOException e )
        {
            System.out.println( e.getMessage() );
        }
    }

    public void readFile ()
    {
        File file = new File (this.fileName);
        String str;
        try
        {
            /*
              TODO: Conseguir guardar los atributos de una persona
                   name, lastName... en el ArrayList<Person>
             */
            FileReader     fileR  = new FileReader( file );
            BufferedReader fileBR = new BufferedReader( fileR );

            str = fileBR.readLine();
            while( str != null )
            {
                System.out.println( str );
                str = fileBR.readLine();
            }
            fileBR.close();
        } catch ( IOException e)
        {
            System.out.println( e.getMessage() );
        }
    }
}

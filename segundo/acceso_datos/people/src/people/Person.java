package people;

public class Person
{
    // Atributos comunes de personas
    private String name, lastName, country;
    private int age;

    // Constructor
    public Person ( String name, String lastName, int age, String country )
    {
        this.name       = name;
        this.lastName   = lastName;
        this.age        = age;
        this.country    = country;
    }

    // Getters, optional
    public String getName ()
    {
        return this.name;
    }

    public String getLastName ()
    {
        return this.lastName;
    }

    public String getCountry ()
    {
        return this.country;
    }

    public int getAge ()
    {
        return this.age;
    }

    // Sobreescritura del metodo toString() (lo usaremos para escribir
    // en el fichero)
    @Override
    public String toString ()
    {
        String str =
                name     + "\n" +
                lastName + "\n" +
                country  + "\n" +
                age      + "\n" +
                "@"      + "\n";
        return str;
    }

}

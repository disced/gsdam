package agencia;

import java.io.IOException;
import java.util.Scanner;

public class Agencia
{
    private static String titulo = 
                        "\n\t\t\t##########################" + "\n" +
                        "\t\t\t####  AGENCIA VIAJES  ####" + "\n" + 
                        "\t\t\t##########################" + "\n\n";
    private static String menu   =
            "\t1.-  Leer Fichero" + "\n" + 
            "\t2.-  Insertar Itinerario" + "\n" +
            "\t3.-  Eliminar Itinerario" + "\n" + 
            "\t4.-  Vaciar todos los datos" + "\n" + 
            "\t5.-  Modificar" + "\n" +
            "\t6.-  Guardar Fichero" + "\n" + 
            "\t7.-  Separar en 2 Ficheros" + "\n" + 
            "\t8.-  Mostrar Contenido RAM" + "\n" + 
            "\t9.-  Mostrar Itinerarios de Menor a Mayor" + "\n" + 
            "\t10.- Mostrar Itinerarios de Mayor a Menor" + "\n" + 
            "\t11.- Destino mas repetido" + "\n" + 
            "\n\t12.- Salir\n\n";
    
    private static Scanner sc;

    private static void notifica ( String notificacion )
    {
        System.out.println( "\n\n" + notificacion + "\nEnter para continuar..." );
        sc.nextLine();
    }

    public static void main ( String[] args )
    {
        int          opcUsuario;
        boolean      seguir = true;
        OpcionesMenu opc    = new OpcionesMenu();
        sc = new Scanner( System.in );

        while( seguir )
        {
            System.out.println( titulo + menu );
            System.out.println( "\t\t\tESPERANDO OPCIÓN DEL MENÚ" );
            try
            {
                sc.reset();
                opcUsuario = Integer.parseInt( sc.nextLine() );
            }catch( Exception e )
            {
                opcUsuario = 100;
            }
            switch (opcUsuario)
            {
                case 1:
                    opc.leeGuarda();
                    notifica( "Fichero Leido!" );
                break;

                case 2:
                    opc.insertaItinerario();
                    notifica( "Itinerario Insertado!" );
                break;

                case 3:
                    opc.eliminaItinerario();
                    notifica( "Itinerario Eliminado!" );
                break;

                case 4:
                    opc.vaciaRam();
                    notifica( "RAM Limpia!" );
                break;

                case 5:
                    System.out.println( "Cambiar Destino de Itinerario\nDestino a Cambiar:" );
                    String dest = sc.nextLine();
                    System.out.println( "Itinerario al que pertenecía :" + dest );
                    String itiViejo = sc.nextLine();
                    System.out.println( "Itinerario nuevo de " + dest + " :" );
                    String itiNuevo = sc.nextLine();

                    opc.modificaItinerario( dest, itiViejo, itiNuevo );
                    notifica( "Destino Cambiado!" );
                break;

                case 6:
                    System.out.println( "\nFichero donde escribir\n" );
                    try
                    {
                        opc.guardaFichero( sc.nextLine() );
                        notifica( "Fichero Escrito!" );
                    }catch( IOException e )
                    {
                        e.printStackTrace();
                    }
                break;

                case 7:
                    try
                    {
                        opc.separarFichero();
                        notifica( "Dos Ficheros Creados!" );
                    }catch( IOException e )
                    {
                        e.printStackTrace();
                    }
                break;
                case 8:
                    System.out.println( opc.leerDatosRam() );
                    notifica( "" );
                break;
                case 9:
                    opc.mostrarPorOrden( TOrden.MENOR );
                    notifica( "Mostrado de Menor a Mayor" );
                break;
                case 10:
                    opc.mostrarPorOrden( TOrden.MAYOR );
                    notifica( "Mostrado de Mayor a Menor" );
                break;
                case 11:
                    opc.destinoRepetido();
                    notifica( "Mostrado destino más repetido" );
                break;

                case 12:
                    System.out.println( "\nSaliendo..." );
                    sc.close();
                    seguir = false;
                break;
                default:
                    System.out.println( "\n\t\t\tOPCIÓN INVALIDA" );
                    try
                    {
                        Thread.sleep( 999 );
                    }catch( InterruptedException e )
                    {
                        e.printStackTrace();
                    }
                break;
            }
        }

    }

}
